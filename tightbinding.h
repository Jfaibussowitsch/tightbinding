#ifndef TIGHTBINDING_H
#define TIGHTBINDING_H

#include <slepc.h>

#ifndef LAMMPS_EXCEPTIONS
/* Compiler can't know about LAMMPS defines since LAMMPS doesn't expose them, so we
   define this stuff here */
#define LAMMPS_EXCEPTIONS
#include "library.h"
#endif /* LAMMPS_EXCEPTIONS */

#define PETSC_SQRT3  PetscRealConstant(1.7320508075688772935274463415058724)
#define PETSC_KB     PetscRealConstant(0.000086173324)
#define PETSC_RESET  "\x1b[0m"
#define PETSC_YELLOW "\x1b[01;33m"

#define LMPERRQ(lmp)  do{PetscErrorCode lmperr__ = ((PetscErrorCode)lammps_has_error(lmp)); if (PetscUnlikely(lmperr__)) return PetscError(PETSC_COMM_SELF,__LINE__,PETSC_FUNCTION_NAME,__FILE__,lmperr__,PETSC_ERROR_REPEAT," ");} while (0)
#define LMPERRV(lmp)  do{PetscErrorCode lmperr__ = ((PetscErrorCode) lammps_has_error(lmp)); if (PetscUnlikely(lmperr__)) return;} while (0)

PETSC_EXTERN void ComputeTotalEnergy(void*,long long,int,int*,double**,double**);
PETSC_EXTERN PetscErrorCode HamiltonianInteractionKernelOffDiagonal_WANG(void*,Mat,Vec,Vec,Vec,const PetscInt,const PetscInt,const PetscInt,const PetscInt[]);
PETSC_EXTERN PetscErrorCode HamiltonianInteractionKernelDiagonal_WANG(void *,Mat,Vec,const PetscInt,const PetscInt,const PetscInt[]);
PETSC_EXTERN PetscErrorCode RepelEnergyContributionKernel_WANG(void *,Vec,Vec,Vec,const PetscInt, const PetscInt);

#endif /* TIGHTBINDING_H */
