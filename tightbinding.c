static char help[] = "Tightbinding Code For Coupled Energy Computation with LAMMPS\n";

#include "tightbinding.h"

/* ------------------------------------------------------
          User Context For Storing "Global" Data
   ------------------------------------------------------ */

typedef struct {
  /* FunctionList for all the kernels */
  PetscFunctionList flistDiag, flistOffDiag, flistRepel;
  char              fnameDiag[PETSC_MAX_PATH_LEN], fnameOffDiag[PETSC_MAX_PATH_LEN], fnameRepel[PETSC_MAX_PATH_LEN];
  /* LAMMPS Object */
  void         *lmp;
  /* FixExternal Data */
  Mat          H;
  EPS          eps;
  char         feid[PETSC_MAX_PATH_LEN], fetefname[PETSC_MAX_PATH_LEN];
  FILE         *fetefp;
  /* Command Line Options Flags */
  PetscBool    lammpsfileflg, neighview, neighset;
  PetscInt     lammpsexceptions;
  PetscReal    LatticeConstant, rCut, EnergyRepel;
#if (0)
  PetscBool    latticefileflg, LammpsView, hex, lammpsNTStepsSet;
  PetscInt     dim, nAtoms, AtomTypeIter, ThermoDumpInterval, lammpsNTSteps, AtomDumpInterval;
  PetscInt     RepNum[3];
  PetscReal    mass;
  PetscReal    UnitVecComp[9], BasisVecComp[12], RegionBounds[6], TriclinicShift[3];
  char         latticefilename[PETSC_MAX_PATH_LEN], ThermoArgs[PETSC_MAX_PATH_LEN], AtomViewFormat[PETSC_MAX_PATH_LEN];
  char         **xyz_periodicity;
#endif
  /* LAMMPS File */
  char         lammpsinfilename[PETSC_MAX_PATH_LEN];
} AppCtx;

/* TODO Redo, should integrate into petscerrorhandler() */
static PetscErrorCode LMPERR(void *lmp)
{
  PetscErrorCode  lmp_err, ierr;

  PetscFunctionBegin;
  lmp_err = (PetscErrorCode) lammps_has_error(lmp);
  if (PetscUnlikely(lmp_err)) {
    char  lmp_err_mess[PETSC_MAX_PATH_LEN];

    lmp_err = (PetscErrorCode) lammps_get_last_error_message(lmp, lmp_err_mess, PETSC_MAX_PATH_LEN);
    ierr = PetscPrintf(PETSC_COMM_SELF, "ERROR CODE: %D\n", lmp_err);CHKERRQ(ierr);
    if (lmp_err == 1) {
      SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_LIB, "LAMMPS has returned RECOVERABLE error code %D with message %s. To disable exit after recoverable LAMMPS errors run with option -ignore_recoverable_errors", lmp_err, lmp_err_mess);
    } else if (lmp_err == 2) {
      SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_LIB, "LAMMPS has returned FATAL error code %D with message %s", lmp_err, lmp_err_mess);
    } else {
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "LAMMPS has returned error without error message");
    }
  }
  PetscFunctionReturn(0);
}

// TODO Needs some serious cleaning up and de-crufting
static PetscErrorCode ProcessOpts(MPI_Comm comm, AppCtx *user, PetscBool *end)
{
  PetscErrorCode  ierr;
  PetscBool	  helptb = PETSC_FALSE, helptbset = PETSC_FALSE, fetefnameset = PETSC_FALSE;
#if (0)
  PetscInt        numUnitVecComp = 3, numBasisVecComp = 3,  numRegionBounds = 2, numTriclinicShift = 3, numPeriodicities = 3, numRepNum = 3;
  PetscBool       aXVecSet = PETSC_FALSE, bXVecSet = PETSC_FALSE;
#endif


  PetscFunctionBegin;
  ierr = PetscStrcpy(user->feid, "ext");CHKERRQ(ierr);
  user->lmp                 = NULL;
  user->H                   = NULL;
  user->eps                 = NULL;
  user->flistDiag           = NULL;
  user->flistOffDiag        = NULL;
  user->flistRepel          = NULL;
  user->lammpsexceptions    = 0;
  user->lammpsfileflg       = PETSC_FALSE;
  user->neighview           = PETSC_FALSE;
  user->neighset            = PETSC_FALSE;
  user->LatticeConstant     = 2.4595;
  user->rCut                = 1.0;
  user->EnergyRepel         = 0.0;
  user->lammpsinfilename[0] = '\0';
  user->fetefname[0]        = '\0';
  user->fetefp              = NULL;
#if (0)
  ierr = PetscArrayzero(user->UnitVecComp, 3*numUnitVecComp);CHKERRQ(ierr);
  ierr = PetscArrayzero(user->BasisVecComp, 4*numBasisVecComp);CHKERRQ(ierr);
  ierr = PetscArrayzero(user->RegionBounds, 3*numRegionBounds);CHKERRQ(ierr);
  ierr = PetscArrayzero(user->TriclinicShift, numTriclinicShift);CHKERRQ(ierr);
  user->latticefileflg      = PETSC_FALSE;
  user->LammpsView          = PETSC_FALSE;
  user->hex                 = PETSC_TRUE;
  user->lammpsNTStepsSet    = PETSC_FALSE;
  user->dim                 = 2;
  user->nAtoms              = 64;
  user->ThermoDumpInterval  = 1;
  user->AtomDumpInterval    = 1;
  user->AtomTypeIter        = 1;
  user->lammpsNTSteps       = PETSC_DEFAULT;
  user->mass                = 12.0107;
  user->RepNum[0]           = 1;
  user->RepNum[1]           = 1;
  user->RepNum[2]           = 1;
  user->UnitVecComp[0]      = 0.8660296808;
  user->UnitVecComp[1]      = 0.5;
  user->UnitVecComp[4]      = 1.0;
  user->UnitVecComp[8]      = 1.0;
  user->BasisVecComp[3]     = 0.5;
  user->BasisVecComp[4]     = 0.28867513459481287;
  user->RegionBounds[1]     = 1;
  user->RegionBounds[3]     = 1;
  user->RegionBounds[5]     = 1;
  user->TriclinicShift[0]   = 0;
  user->latticefilename[0]  = '\0';
  user->ThermoArgs[0]       = '\0';
  user->AtomViewFormat[0]   = '\0';
  user->xyz_periodicity[0]  = NULL;
  user->xyz_periodicity[1]  = NULL;
  user->xyz_periodicity[2]  = NULL;
#endif

  /* Register functions	available on the command-line */
  ierr = PetscFunctionListAdd(&user->flistDiag, "wang", HamiltonianInteractionKernelDiagonal_WANG);CHKERRQ(ierr);
  ierr = PetscFunctionListAdd(&user->flistOffDiag, "wang", HamiltonianInteractionKernelOffDiagonal_WANG);CHKERRQ(ierr);
  ierr = PetscFunctionListAdd(&user->flistRepel, "wang", RepelEnergyContributionKernel_WANG);CHKERRQ(ierr);
  ierr = PetscStrcpy(user->fnameDiag, "wang");CHKERRQ(ierr);
  ierr = PetscStrcpy(user->fnameOffDiag, "wang");CHKERRQ(ierr);
  ierr = PetscStrcpy(user->fnameRepel, "wang");CHKERRQ(ierr);

  /* Check so that we activate help logging */
  ierr = PetscOptionsBegin(comm, NULL, "Tightbinding Options", "");CHKERRQ(ierr);
  {
    /* In here so that it shows up on options list */
    ierr = PetscOptionsGetBool(NULL, NULL, "-help_tightbinding", &helptb, &helptbset);CHKERRQ(ierr);
    /* Total hack, you may be wondering "where is PetscOptionsObjectBase, this isn't
       declared anywhere I can see"?? And you would be right. But its declared in
       PetscOptionsBegin which is secretly a macro... In fact since we directly change
       object internals through this "back door", PetscOptionsHasHelp will return false
       since the options monitor is completely unchanged */
    if (helptb && helptbset) {PetscOptionsObjectBase.printhelp = PETSC_TRUE;}
    ierr = PetscOptionsBool("-help_tightbinding", "Show only tightbinding help", "tightbinding.c", helptb, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscOptionsFList("-hamiltonian_diagonal", "Function to compute Hamiltonian diagonal blocks", NULL, user->flistDiag, user->fnameDiag, user->fnameDiag, PETSC_MAX_PATH_LEN, NULL);CHKERRQ(ierr);
    ierr = PetscOptionsFList("-hamiltonian_off_diagonal", "Function to compute Hamiltonial off diagonal blocks", NULL, user->flistOffDiag, user->fnameOffDiag, user->fnameOffDiag, PETSC_MAX_PATH_LEN, NULL);CHKERRQ(ierr);
    ierr = PetscOptionsFList("-total_energy_repel_contribution", "Function to compute repel contributions of total energy", NULL, user->flistRepel, user->fnameRepel, user->fnameRepel, PETSC_MAX_PATH_LEN, NULL);CHKERRQ(ierr);
    ierr = PetscOptionsString("-lammps_input_filename", "LAMMPS input script file", NULL, user->lammpsinfilename, user->lammpsinfilename, PETSC_MAX_PATH_LEN, &user->lammpsfileflg); CHKERRQ(ierr);
    ierr = PetscOptionsString("-total_energy_view", "Total Energy Output File", NULL, user->fetefname, user->fetefname, PETSC_MAX_PATH_LEN, &fetefnameset); CHKERRQ(ierr);
    ierr = PetscOptionsString("-lammps_fixexternal_id", "Alphanumeric external fix ID", NULL, user->feid, user->feid,  PETSC_MAX_PATH_LEN, NULL); CHKERRQ(ierr);
    ierr = PetscOptionsBool("-lammps_neighbor_list_view", "LAMMPS Output neighbor list format", "PetscViewerASCII", user->neighview, &user->neighview, &user->neighset);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-lammps_lattice_constant", "LAMMPS lattice constant", "https://lammps.sandia.gov/doc/lattice.html", user->LatticeConstant, &user->LatticeConstant, NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-lammps_cutting_radius", "Cutting radius for atom pair interactions", NULL, user->rCut, &user->rCut, NULL);CHKERRQ(ierr);
  }
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  user->lammpsexceptions = (PetscInt) lammps_config_has_exceptions();
  /* Gracefully end program if special -help_tightbinding is provided */
  if (helptb && helptbset) {
    ierr = PetscOptionsClearValue(NULL, "-help");CHKERRQ(ierr);
    *end = PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  if (!user->lammpsfileflg) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER_INPUT,"Must supply a lammps input file script. -lammps_input_filename not set!");
  /* Open total energy output file */
  if (fetefnameset) {
    ierr = PetscFOpen(comm, user->fetefname[0] ? user->fetefname : NULL, "w", &user->fetefp);CHKERRQ(ierr);
  }

  /* Test viability of all of the provided functions */
  {
    PetscErrorCode (*testDiag)(AppCtx*,Mat,Vec,const PetscInt,const PetscInt,const PetscInt []);
    ierr = PetscFunctionListFind(user->flistDiag, user->fnameDiag, &testDiag);CHKERRQ(ierr);
    if (!testDiag) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_USER_INPUT, "Provided function '%s' non-existent or not registered. Check spelling and make sure you have called PetscFunctionListAdd()", user->fnameDiag);
  }
  {
    PetscErrorCode (*testOffDiag)(AppCtx*,Mat,Vec,Vec,Vec,const PetscInt,const PetscInt,const PetscInt,const PetscInt []);
    ierr = PetscFunctionListFind(user->flistOffDiag, user->fnameOffDiag, &testOffDiag);CHKERRQ(ierr);
    if (!testOffDiag) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_USER_INPUT, "Provided function '%s' non-existent or not registered. Check spelling and make sure you have called PetscFunctionListAdd()", user->fnameOffDiag);
  }
  {
    PetscErrorCode (*testRepel)(AppCtx*,Vec,Vec,Vec,const PetscInt,const PetscInt);
    ierr = PetscFunctionListFind(user->flistRepel, user->fnameRepel, &testRepel);CHKERRQ(ierr);
    if (!testRepel) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_USER_INPUT, "Provided function '%s' non-existent or not registered. Check spelling and make sure you have called PetscFunctionListAdd()", user->fnameRepel);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode PostProcessOpts(MPI_Comm comm, AppCtx *user)
{
  PetscErrorCode  ierr;
  PetscBool       hasHelp;

  PetscFunctionBegin;
  ierr = PetscFunctionListDestroy(&user->flistDiag);CHKERRQ(ierr);
  ierr = PetscFunctionListDestroy(&user->flistOffDiag);CHKERRQ(ierr);
  ierr = PetscFunctionListDestroy(&user->flistRepel);CHKERRQ(ierr);
  if (user->fetefname[0]) {
    ierr = PetscFClose(comm, user->fetefp);CHKERRQ(ierr);
  }
  ierr = PetscOptionsHasHelp(NULL, &hasHelp);CHKERRQ(ierr);
  if (hasHelp) {
    PetscMPIInt rank;

    ierr = MPI_Comm_rank(comm,  &rank);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "%s[%d]WARNING: ---------------------------------------- Warning Message ----------------------------------------%s\n", PETSC_YELLOW, rank, PETSC_RESET);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "[%d]WARNING: -help PRINTS ALL OPTIONS!\n", rank);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "[%d]WARNING: Tightbinding options are at the top, or use -help_tightbinding to only display tightbinding options\n", rank);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "%s[%d]WARNING: ---------------------------------------- Warning Message ----------------------------------------%s\n", PETSC_YELLOW, rank, PETSC_RESET);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode LammpsProcessCommand(void *lmp, char *CmdStr[], const char format[],...)
{
  PetscErrorCode  ierr;
  size_t          CmdStrLen;
  va_list         Argp;

  PetscFunctionBegin;
  va_start(Argp,format);
  ierr = PetscVSNPrintf(*CmdStr, PETSC_MAX_PATH_LEN, format, &CmdStrLen, Argp);CHKERRQ(ierr);
  PetscStackCall("lammps_command", lammps_command(lmp, *CmdStr));LMPERRQ(lmp);
  ierr = PetscArrayzero(*CmdStr, CmdStrLen);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#if (0)
// TODO remove
static PetscErrorCode LammpsSetUp(AppCtx *user)
{
  PetscErrorCode  ierr;
  char            *CmdStr;

  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_LIB, "This should be phased out in favor of a LAMMPS '.in' file");
  ierr = PetscCalloc1(user->sizetData, &CmdStr);CHKERRQ(ierr);
  /* Setup Simulation */
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "units real");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "dimension %d", user->dim);CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "boundary %s %s %s", user->xyz_periodicity[0], user->xyz_periodicity[1], user->xyz_periodicity[2]);CHKERRQ(ierr);
  /* Define the lattice */
  if (user->latticefileflg) {
    ierr = LammpsProcessCommand(user->lmp, &CmdStr, "read_data %s", user->latticefilename);CHKERRQ(ierr);
  } else {
    ierr = LammpsProcessCommand(user->lmp, &CmdStr, "region customR prism %f %f %f %f %f %f %f %f %f", user->RegionBounds[0], user->RegionBounds[1], user->RegionBounds[2], user->RegionBounds[3], user->RegionBounds[4], user->RegionBounds[5], user->TriclinicShift[0], user->TriclinicShift[1], user->TriclinicShift[2]);CHKERRQ(ierr);
    ierr = LammpsProcessCommand(user->lmp, &CmdStr, "create_box 1 customR");CHKERRQ(ierr);
    if(user->hex) {
      PetscInt i;

      for (i = 0; i < 4; i++) {
	ierr = LammpsProcessCommand(user->lmp, &CmdStr, "create_atoms 1 single %f %f %f", user->BasisVecComp[3*i], user->BasisVecComp[(3*i)+1], user->BasisVecComp[(3*i)+2]);CHKERRQ(ierr);
      }
      ierr = LammpsProcessCommand(user->lmp, &CmdStr, "replicate %d %d %d", user->RepNum[0], user->RepNum[1], user->RepNum[2]);CHKERRQ(ierr);
    } else {
      ierr = LammpsProcessCommand(user->lmp, &CmdStr, "lattice custom %f a1 %f %f %f a2 %f %f %f a3 %f %f %f basis %f %f %f basis %f %f %f", user->LatticeConstant, user->UnitVecComp[0], user->UnitVecComp[1], user->UnitVecComp[2], user->UnitVecComp[3], user->UnitVecComp[4], user->UnitVecComp[5], user->UnitVecComp[6], user->UnitVecComp[7], user->UnitVecComp[8], user->BasisVecComp[0], user->BasisVecComp[1], user->BasisVecComp[2], user->BasisVecComp[3], user->BasisVecComp[4], user->BasisVecComp[5]);CHKERRQ(ierr);
      ierr = LammpsProcessCommand(user->lmp, &CmdStr, "create_atoms 1 region customR units lattice");CHKERRQ(ierr);
    }
  }
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "mass * %f", user->mass);CHKERRQ(ierr);
  //ierr = LammpsProcessCommand(user, &CmdStr, "pair_coeff * * /Users/jacobfaibussowitsch/NoSync/lammps/potentials/CCu_v2.bop.table C");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "variable a1 equal lx/3");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "variable a2 equal ly/sqrt(3)");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "thermo_style custom %s", user->ThermoArgs[0] ? user->ThermoArgs : "step etotal v_a1 v_a2");CHKERRQ(ierr);
  //ierr = LammpsProcessCommand(user, &CmdStr, "compute 1 all bond/local
  //dist");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "timestep 0.00001");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "min_style cg");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "minimize 1.0e-4 1.0e-6 100 1000");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "fix dynamics all nve");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "thermo %d", user->ThermoDumpInterval);CHKERRQ(ierr);
  ierr = PetscFree(CmdStr);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
#endif

static PetscErrorCode LammpsReadFileBegin(MPI_Comm comm, AppCtx *user, const char fname[], FILE **fp, char **computeCmd)
{
  PetscErrorCode  ierr;
  char            data[PETSC_MAX_PATH_LEN];
  char            *CmdStr;
  PetscBool       isrun, ismin;

  PetscFunctionBegin;
  ierr = PetscFOpen(comm, fname, "r", fp);CHKERRQ(ierr);
  if (!fp) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_FILE_OPEN, "Can't open LAMMPS script %s\n", fname);
  ierr = PetscCalloc1(PETSC_MAX_PATH_LEN, &CmdStr);CHKERRQ(ierr);
  while (PETSC_TRUE) {
    ierr = PetscSynchronizedFGets(comm, *fp, PETSC_MAX_PATH_LEN, data);CHKERRQ(ierr);
    if (*fp && feof(*fp)) {
      SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_FILE_UNEXPECTED, "LAMMPS script %s has no 'run' or 'minimize' command. There can be no leading spaces in the 'run' or 'minimize' command", fname);
      break;
    }
    ierr = PetscStrncmp(data, "run", 3, &isrun);CHKERRQ(ierr);
    ierr = PetscStrncmp(data, "minimize", 8, &ismin);CHKERRQ(ierr);
    if (isrun || ismin) {
      ierr = PetscStrallocpy(data, computeCmd);CHKERRQ(ierr);
      break;
    }
    ierr = LammpsProcessCommand(user->lmp, &CmdStr, data);CHKERRQ(ierr);
  }
  ierr = PetscFree(CmdStr);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode LammpsReadFileEnd(MPI_Comm comm, AppCtx *user, FILE **fp, char **computeCmd)
{
  PetscErrorCode  ierr;
  char            data[PETSC_MAX_PATH_LEN];
  char            *CmdStr;
  PetscBool	  breakall = PETSC_FALSE;

  PetscFunctionBegin;
  if (!fp) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_NULL,"Null File Pointer");
  ierr = PetscCalloc1(PETSC_MAX_PATH_LEN, &CmdStr);CHKERRQ(ierr);
  while (!breakall) {
    ierr = PetscSynchronizedFGets(comm, *fp, PETSC_MAX_PATH_LEN, data);CHKERRQ(ierr);
    if (*fp && feof(*fp)) breakall = PETSC_TRUE;
    ierr = MPI_Bcast(&breakall, 1, MPIU_BOOL, 0, comm);CHKERRQ(ierr);
    ierr = LammpsProcessCommand(user->lmp, &CmdStr, data);CHKERRQ(ierr);
  }
  if (computeCmd) {ierr = PetscFree(*computeCmd);CHKERRQ(ierr);}
  ierr = PetscFClose(comm, *fp);CHKERRQ(ierr);
  ierr = PetscFree(CmdStr);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode LammpsForceNeighListConstruction(AppCtx *user)
{
  PetscErrorCode  ierr;
  char            *CmdStr;

  PetscFunctionBegin;
  ierr = PetscCalloc1(PETSC_MAX_PATH_LEN, &CmdStr);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "=======================================\n  FORCING NEIGHBOR LIST CONSTRUCTION\n=======================================\n");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "neighbor 1.0 bin");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "neigh_modify every 1 delay 0 check no");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "pair_style zero %f nocoeff", user->rCut);CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "pair_coeff * *");CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user->lmp, &CmdStr, "run 0");CHKERRQ(ierr);
  ierr = PetscFree(CmdStr);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode LammpsExtractData(AppCtx *user, PetscInt *nGlobalAtoms, PetscInt **globalIDs, PetscInt *nLocalAtoms, PetscInt *nGhostAtoms, PetscInt *neighListId, PetscInt *numNeighListElems, PetscReal **contigCoords)
{
  PetscFunctionBegin;
  if (nLocalAtoms) {*nLocalAtoms = *(PetscInt *)lammps_extract_global(user->lmp, (char *) "nlocal");LMPERR(user->lmp);}
  if (nGhostAtoms) {*nGhostAtoms = *(PetscInt *)lammps_extract_global(user->lmp, (char *) "nghost");LMPERR(user->lmp);}
  if (nGlobalAtoms) {*nGlobalAtoms = *(PetscInt *)lammps_extract_global(user->lmp, (char *) "natoms");LMPERR(user->lmp);}
  if (globalIDs) {*globalIDs = (PetscInt *)lammps_extract_atom(user->lmp, (char *)"id");LMPERR(user->lmp);}
  if (neighListId) {
    *neighListId = (PetscInt)lammps_find_pair_neighlist(user->lmp, (char *) "zero", 1, 1, 0);LMPERR(user->lmp);
    if (*neighListId == -1) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "Neighbor list could not be found! Possible that it wasn't generated");
    if (numNeighListElems) {
      *numNeighListElems = (PetscInt)lammps_neighlist_num_elements(user->lmp, *neighListId);LMPERR(user->lmp);
    }
  }
  if (contigCoords) {
    PetscInt        i, j, cmplxOff = 1, cDim = 3;
    double          **coords;
    PetscErrorCode  ierr;

#if defined(PETSC_USE_COMPLEX)
    cmplxOff = 2;
#endif
    coords = (double **)lammps_extract_atom(user->lmp, (char *) "x");LMPERR(user->lmp);
    ierr = PetscCalloc1(cmplxOff*cDim*(*nLocalAtoms+*nGhostAtoms), contigCoords);CHKERRQ(ierr);
    for (i = 0; i < (*nLocalAtoms+*nGhostAtoms); i++) {
      for (j = 0; j < cDim; j++) {
	(*contigCoords)[(i*cDim*cmplxOff)+(j*cmplxOff)] = (PetscReal) (coords[i][j]);
      }
    }
  }
  PetscFunctionReturn(0);
}

#if (0)
// TODO should really be removed
static PetscErrorCode LammpsViewAtomsFromOptions(AppCtx *user)
{
  PetscFunctionBegin;
  if (user->LammpsView) {
    PetscErrorCode  ierr;
    PetscBool       isascii = PETSC_TRUE;

    if (isascii) {
      PetscViewer  asciivwr;
      PetscMPIInt  rank;
      PetscInt     nGlobalAtoms, nLocalAtoms, nGhostAtoms, i;
      PetscInt     *globalIDs;
      PetscReal    *contigCoords;

      ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
      ierr = LammpsExtractData(user, &nGlobalAtoms, &globalIDs, &nLocalAtoms, &nGhostAtoms, NULL, NULL, &contigCoords);CHKERRQ(ierr);
      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, user->AtomViewFormat, &asciivwr);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(asciivwr, "=======================================\n        Lammps Atom View\n=======================================\n");CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(asciivwr, "General Info:\nGlobal Atoms: %D\n", nGlobalAtoms);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPushSynchronized(asciivwr);CHKERRQ(ierr);
      ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "[%d] Local Atoms: %D\n", rank, nLocalAtoms);CHKERRQ(ierr);
      ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "[%d] Local Ghost Atoms: %D\n", rank, nGhostAtoms);CHKERRQ(ierr);
      ierr = PetscViewerFlush(asciivwr);CHKERRQ(ierr);
      ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "\n[%d] Coordinates Local:\n", rank);CHKERRQ(ierr);
      for(i = 0; i < nLocalAtoms; i++) {
	ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "[%d] %.5f, %.5f, %.5f\n", rank, contigCoords[i], contigCoords[i+1], contigCoords[i+2]);CHKERRQ(ierr);
      }
      ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "\n[%d] Coordinates Ghost:\n", rank);CHKERRQ(ierr);
      for(i = nLocalAtoms; i < (nLocalAtoms+nGhostAtoms); i++) {
	ierr = PetscViewerASCIISynchronizedPrintf(asciivwr, "[%d] %.5f, %.5f, %.5f\n", rank, contigCoords[i], contigCoords[i+1], contigCoords[i+2]);CHKERRQ(ierr);
      }
      ierr = PetscViewerFlush(asciivwr);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopSynchronized(asciivwr);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&asciivwr);CHKERRQ(ierr);
    } else {
      char  *CmdStr;

      user->sizetData = 128;
      ierr = PetscCalloc1(user->sizetData, &CmdStr);CHKERRQ(ierr);
      ierr = LammpsProcessCommand(user, &CmdStr, "dump d0 all image %d dump.*.jpg type type", user->AtomDumpInterval);CHKERRQ(ierr);
      ierr = PetscFree(CmdStr);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}
#endif

/*
  TODO should clean up and generalize, perhaps wrap it in a Vec so we can output with a
  lot of different formats.
*/
static PetscErrorCode LammpsNeighborViewFromOptions(AppCtx *user, MPI_Comm comm)
{
  PetscErrorCode  ierr;
  PetscMPIInt     rank;
  PetscInt        nGlobalAtoms, nLocalAtoms, nGhostAtoms, neighListId, numNeighListElems, i, j, cDim = 3;
  PetscInt        *globalIDs;
  PetscReal	  *contigCoords;
  int             *neighs;
  int             numneigh, atomidx;

  PetscFunctionBegin;
  if (user->neighset && user->neighview) {
    ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "=======================================\n Lammps Neighbor-list View\n=======================================\n");CHKERRQ(ierr);
    ierr = LammpsExtractData(user, &nGlobalAtoms, &globalIDs, &nLocalAtoms, &nGhostAtoms, &neighListId, &numNeighListElems, &contigCoords);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "Num Global:   %4D\n", nGlobalAtoms);CHKERRQ(ierr);
    ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD, "%D: Num Local: %4D\n%D: Num Ghost: %4D\n", rank, nLocalAtoms, rank, nGhostAtoms);CHKERRQ(ierr);
    ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD, NULL);CHKERRQ(ierr);
    for (i = 0; i < nLocalAtoms; i++) {
      lammps_neighlist_element_neighbors(user->lmp, (int) neighListId, (int) i, &atomidx, &numneigh, &neighs);LMPERR(user);
      ierr = PetscSynchronizedPrintf(comm, "%D: Atom [GLOB %3D, LOC %3D]: (%3.3f, %3.3f, %3.3f) Num Neighbors -> %D\n", rank, globalIDs[i], atomidx, contigCoords[cDim*i], contigCoords[(cDim*i)+1], contigCoords[(cDim*i)+2], numneigh);CHKERRQ(ierr);
      for (j = 0; j < numneigh; j++) {
	ierr = PetscSynchronizedPrintf(comm, "    Neighbor %D: local idx %D -> (%3.3f, %3.3f, %3.3f)\n", j, neighs[j], contigCoords[cDim*neighs[j]], contigCoords[(cDim*neighs[j])+1], contigCoords[(cDim*neighs[j])+2]);CHKERRQ(ierr);
      }
    }
    ierr = PetscSynchronizedFlush(comm, NULL);CHKERRQ(ierr);
    ierr = PetscFree(contigCoords);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------
   Define All Optional Functions Here And In Tighbinding.h
   ------------------------------------------------------ */

PetscErrorCode HamiltonianInteractionKernelOffDiagonal_WANG(void *ctx, Mat H, Vec sVec, Vec nVec, Vec dVec, const PetscInt localai, const PetscInt localaj, const PetscInt nLocalGlobalIDs, const PetscInt localGlobalIDs[])
{
  PetscScalar        Vss_sigma = -8.42256, Vsp_sigma = 8.08162, Vpp_sigma = 7.75792, Vpp_pi = -3.67510, r0 = 1.312, na_ss_sigma = 1.29827, na_sp_sigma = 0.99055, na_pp_sigma = 1.01545, na_pp_pi = 1.82460, nc = 5.0, nb = 1.0, rt = 2.00;
  PetscScalar        Es[4], Ex[4], Ey[4], Ez[4];
  const PetscScalar  *lmn;
  PetscReal          r;
  PetscInt           idxm[1] = {localGlobalIDs[localai]-1}, idxn[1] = {localGlobalIDs[localai]-1};
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  ierr = VecNormalize(dVec, &r);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dVec, &lmn);CHKERRQ(ierr);
  Vss_sigma = PetscPowComplex(Vss_sigma*(r0/r), na_ss_sigma)*PetscExpComplex(PetscPowComplex(-nb*(r/rt), nc) + PetscPowComplex(nb*(r0/rt), nc));
  Vsp_sigma = PetscPowComplex(Vsp_sigma*(r0/r), na_sp_sigma)*PetscExpComplex(PetscPowComplex(-nb*(r/rt), nc) + PetscPowComplex(nb*(r0/rt), nc));
  Vpp_sigma = PetscPowComplex(Vpp_sigma*(r0/r), na_pp_sigma)*PetscExpComplex(PetscPowComplex(-nb*(r/rt), nc) + PetscPowComplex(nb*(r0/rt), nc));
  Vpp_pi = PetscPowComplex(Vpp_pi*(r0/r), na_pp_pi)*PetscExpComplex(PetscPowComplex(-nb*(r/rt), nc) + PetscPowComplex(nb*(r0/rt), nc));

  Es[0] = Vss_sigma;
  Es[1] = lmn[0]*Vsp_sigma;
  Es[2] = lmn[1]*Vsp_sigma;
  Es[3] = lmn[2]*Vsp_sigma;

  Ex[0] = -lmn[0]*Vsp_sigma;
  Ex[1] = (PetscPowComplex(lmn[0], 2)*Vpp_sigma)+((1-PetscPowComplex(lmn[0], 2))*Vpp_pi);
  Ex[2] = (lmn[0]*lmn[1]*Vpp_sigma)-(lmn[0]*lmn[1]*Vpp_pi);
  Ex[3] = (lmn[0]*lmn[2]*Vpp_sigma)-(lmn[0]*lmn[2]*Vpp_pi);

  Ey[0] = -lmn[1]*Vsp_sigma;
  Ey[1] = Ex[2];
  Ey[2] = (PetscPowComplex(lmn[1], 2)*Vpp_sigma)+((1-PetscPowComplex(lmn[1], 2))*Vpp_pi);
  Ey[3] = (lmn[1]*lmn[2]*Vpp_sigma)-(lmn[1]*lmn[2]*Vpp_pi);

  Ez[0] = -lmn[2]*Vsp_sigma;
  Ez[1] = (lmn[0]*lmn[2]*Vpp_sigma)-(lmn[0]*lmn[2]*Vpp_pi);
  Ez[2] = (lmn[1]*lmn[2]*Vpp_sigma)-(lmn[1]*lmn[2]*Vpp_pi);
  Ez[3] = (PetscPowComplex(lmn[2], 2)*Vpp_sigma)+((1-PetscPowComplex(lmn[2], 2))*Vpp_pi);

  const PetscScalar  valmat[16] = {Es[0], Es[1], Es[2], Es[3], Ex[0], Ex[1], Ex[2], Ex[3], Ey[0], Ey[1], Ey[2], Ey[3], Ez[0], Ez[1], Ez[2], Ez[3]};

  if ((localGlobalIDs[localai]) < localGlobalIDs[localaj]) {
    idxm[0] = localGlobalIDs[localai]-1;
    idxn[0] = localGlobalIDs[localaj]-1;
  } else {
    // swap so we insert on upper triangular
    idxn[0] = localGlobalIDs[localai]-1;
    idxm[0] = localGlobalIDs[localaj]-1;
  }
  ierr = MatSetValuesBlocked(H, 1, (const PetscInt *) idxm, 1, (const PetscInt *) idxn, valmat, ADD_VALUES);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(dVec, &lmn);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode HamiltonianInteractionKernelDiagonal_WANG(void *ctx, Mat H, Vec sVec, const PetscInt localai, const PetscInt nLocalGlobalIDs, const PetscInt localGlobalIDs[])
{
  PetscInt           idxm[1] = {localGlobalIDs[localai]-1};
  const PetscScalar  Ed[16] = {-10.290, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  ierr = MatSetValuesBlocked(H, 1, (const PetscInt *) idxm, 1, (const PetscInt *) idxm, (const PetscScalar *) Ed, ADD_VALUES);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RepelEnergyContributionKernel_WANG(void *ctx, Vec sVec, Vec nVec, Vec dVec, const PetscInt localai, const PetscInt localaj)
{
  PetscReal        norm;
  AppCtx           *user = (AppCtx *) ctx;
  const PetscReal  Ecorer0 = 22.68939, r0 = 0.184*user->LatticeConstant, ma = 2.72405, mb = 1.0, mc = 7.0, rc = 1.9;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  ierr = VecNorm(dVec, NORM_2, &norm);CHKERRQ(ierr);
  if (norm > user->rCut) PetscFunctionReturn(0);
  user->EnergyRepel += (Ecorer0*PetscPowReal(r0/norm, ma)*PetscExpReal(-mb*PetscPowReal(norm/rc, mc) + mb*PetscPowReal(r0/rc, mc)))/2;
  PetscFunctionReturn(0);
}

static PetscErrorCode HamiltonianCreate(AppCtx *user, MPI_Comm comm, Mat *H)
{
  PetscErrorCode  ierr;
  PetscInt        nLocalAtoms, neighListId, i;
  int             *intI, *nNeigh, *locAtomID, **neighList;
  int             intNeighListId;
  const PetscInt  bs = 4;

  PetscFunctionBegin;
  ierr = LammpsExtractData(user, NULL, NULL, &nLocalAtoms, NULL, &neighListId, NULL, NULL);CHKERRQ(ierr);
  ierr = PetscMPIIntCast(neighListId, &intNeighListId);CHKERRQ(ierr);
  ierr = PetscMalloc4(nLocalAtoms, &intI, nLocalAtoms, &locAtomID, nLocalAtoms, &nNeigh, nLocalAtoms, &neighList);CHKERRQ(ierr);
  for (i = 0; i < nLocalAtoms; i++) {
    ierr = PetscMPIIntCast(i, &intI[i]);CHKERRQ(ierr);
    lammps_neighlist_element_neighbors(user->lmp, intNeighListId, intI[i], &locAtomID[i], &nNeigh[i], &neighList[i]);LMPERR(user);
  }
  ierr = MatCreateSBAIJ(comm, bs, bs*nLocalAtoms, bs*nLocalAtoms, PETSC_DETERMINE, PETSC_DETERMINE, nLocalAtoms, NULL, PETSC_DEFAULT, (const PetscInt *) nNeigh, H);CHKERRQ(ierr);
  ierr = MatSetOption(*H, MAT_SYMMETRIC, PETSC_TRUE);CHKERRQ(ierr);
  ierr = MatSetOption(*H, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_FALSE);CHKERRQ(ierr);
  ierr = MatSetFromOptions(*H);CHKERRQ(ierr);
  ierr = MatSetUp(*H);CHKERRQ(ierr);
  ierr = PetscFree4(intI, locAtomID, nNeigh, neighList);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode HamiltonianUpdate(AppCtx *user, Mat H)
{
  PetscErrorCode  ierr;
  PetscErrorCode (*diag)(AppCtx*,Mat,Vec,const PetscInt,const PetscInt,const PetscInt []);
  PetscErrorCode (*offDiag)(AppCtx*,Mat,Vec,Vec,Vec,const PetscInt,const PetscInt,const PetscInt,const PetscInt []);
  PetscErrorCode (*repel)(AppCtx*,Vec,Vec,Vec,const PetscInt,const PetscInt);
  Vec             cVec;
  PetscInt        nGlobalAtoms, nLocalAtoms, nGhostAtoms, neighListId, numNeighListElems, i, j;
  PetscInt        *localGlobalIDs;
  const PetscInt  cDim = 3;
  PetscReal       *contigCoords;
  int             intNeighListId, *intI , *locAtomID, *nNeigh, **neighList;

  PetscFunctionBegin;
  ierr = PetscFunctionListFind(user->flistDiag, user->fnameDiag, &diag);CHKERRQ(ierr);
  ierr = PetscFunctionListFind(user->flistOffDiag, user->fnameOffDiag, &offDiag);CHKERRQ(ierr);
  ierr = PetscFunctionListFind(user->flistRepel, user->fnameRepel, &repel);CHKERRQ(ierr);
  ierr = MatZeroEntries(H);CHKERRQ(ierr);
  ierr = LammpsExtractData(user, &nGlobalAtoms, &localGlobalIDs, &nLocalAtoms, &nGhostAtoms, &neighListId, &numNeighListElems, &contigCoords);CHKERRQ(ierr);
  ierr = PetscMPIIntCast(neighListId, &intNeighListId);CHKERRQ(ierr);
  ierr = PetscMalloc4(nLocalAtoms, &intI, nLocalAtoms, &locAtomID, nLocalAtoms, &nNeigh, nLocalAtoms, &neighList);CHKERRQ(ierr);
  for (i = 0; i < nLocalAtoms; i++) {
    ierr = PetscMPIIntCast(i, &intI[i]);CHKERRQ(ierr);
    lammps_neighlist_element_neighbors(user->lmp, intNeighListId, intI[i], &locAtomID[i], &nNeigh[i], &neighList[i]);LMPERR(user);
  }
  ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, cDim, cDim*(nLocalAtoms+nGhostAtoms), (const PetscScalar *) contigCoords, &cVec);CHKERRQ(ierr);
  ierr = VecRealPart(cVec);CHKERRQ(ierr);
  for (i = 0; i < nLocalAtoms; i++) {
    IS                 sIS, nIS;
    Vec		       sVec, nVec, dVec;
    const PetscInt     selfidx[1] = {i};
    const PetscInt     nLocalGlobalIDs = nLocalAtoms+nGhostAtoms;

    ierr = ISCreateBlock(PETSC_COMM_SELF, cDim, 1, selfidx, PETSC_COPY_VALUES, &sIS);CHKERRQ(ierr);
    ierr = VecGetSubVector(cVec, sIS, &sVec);CHKERRQ(ierr);
    ierr = VecDuplicate(sVec, &dVec);CHKERRQ(ierr);
    ierr = (*diag)((void *)user, H, sVec, selfidx[0], nLocalGlobalIDs, localGlobalIDs);CHKERRQ(ierr);
    for (j = 0; j < nNeigh[i]; j++) {
      const PetscInt     neighidx[1] = {(PetscInt) neighList[i][j]};

      ierr = ISCreateBlock(PETSC_COMM_SELF, cDim, 1, neighidx, PETSC_COPY_VALUES, &nIS);CHKERRQ(ierr);
      ierr = VecGetSubVector(cVec, nIS, &nVec);CHKERRQ(ierr);
      ierr = VecWAXPY(dVec, -1.0, sVec, nVec);CHKERRQ(ierr);
      ierr = (*offDiag)((void *)user, H, sVec, nVec, dVec, selfidx[0], neighidx[0], nLocalGlobalIDs, localGlobalIDs);CHKERRQ(ierr);
      ierr = (*repel)((void *)user, sVec, nVec, dVec, selfidx[0], neighidx[0]);CHKERRQ(ierr);
      ierr = VecRestoreSubVector(cVec, nIS, &nVec);CHKERRQ(ierr);
      ierr = ISDestroy(&nIS);CHKERRQ(ierr);
    }
    ierr = VecRestoreSubVector(cVec, sIS, &sVec);CHKERRQ(ierr);
    ierr = VecDestroy(&dVec);CHKERRQ(ierr);
    ierr = ISDestroy(&sIS);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(H, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(H, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = VecDestroy(&cVec);CHKERRQ(ierr);
  ierr = PetscFree5(intI, locAtomID, nNeigh, neighList, contigCoords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

void ComputeTotalEnergy(void *ptr, long long timestep, int nlocal, int *ids, double **x, double **fexternal)
{
  PetscErrorCode  ierr;
  AppCtx          *user = (AppCtx *) ptr;
  EPS             eps = user->eps;
  Mat             H = user->H;
  PetscInt        i, nconv;
  PetscReal       temp, eigr, totalEnergy = 0;
  PetscReal       *extractptrtemp;
  PetscMPIInt     rank;
  MPI_Comm        comm;

  PetscFunctionBegin;
  if (!H || !eps) PetscFunctionReturnVoid();
  user->EnergyRepel = 0;
  ierr = PetscObjectGetComm((PetscObject) H, &comm);CHKERRABORT(comm,ierr);
  ierr = MPI_Comm_rank(comm, &rank);CHKERRABORT(comm,ierr);
  ierr = HamiltonianUpdate(user, H);CHKERRABORT(comm,ierr);
  ierr = MatViewFromOptions(H, NULL, "-hamiltonian_view");CHKERRABORT(comm,ierr);
  ierr = EPSSetOperators(eps, H, NULL);CHKERRABORT(comm,ierr);
  ierr = EPSSolve(eps);CHKERRABORT(comm,ierr);
  /* Although (as usual) undocumented, these lammps extraction methods are collective */
  extractptrtemp = (PetscReal *) lammps_extract_compute(user->lmp, (char *) "thermo_temp", 0, 0);LMPERR(user->lmp);
  if (PetscLikely(extractptrtemp)) temp = *extractptrtemp;
  /* All collective ops have been completed, and only root prints to file */
  if (rank) PetscFunctionReturnVoid();
  ierr = EPSGetConverged(eps, &nconv);CHKERRABORT(comm,ierr);
  for (i = 0; i < nconv; i++) {
    PetscScalar  eig;

    ierr = EPSGetEigenvalue(eps, i, &eig, NULL);CHKERRABORT(comm,ierr);
    eigr = PetscRealPart(eig);
    totalEnergy += eigr/(1+PetscExpReal(eigr/(temp*PETSC_KB)));
  }
  totalEnergy = user->EnergyRepel + (2*totalEnergy);
  if (user->fetefp) {
    ierr = PetscFPrintf(PETSC_COMM_SELF, user->fetefp, "TIMESTEP %D TOTAL ENERGY %f\n", timestep, totalEnergy);CHKERRABORT(comm,ierr);
  }
  /* Uncomment when this get implemented in lammps
  PetscStackCall("lammps_fix_external_set_energy_global",lammps_fix_external_set_energy_global(user->lmp, user->feid, (double) totalEnergy));
  */
  PetscFunctionReturnVoid();
}

int main(int argc, char **argv)
{
  PetscErrorCode  ierr;
  MPI_Comm        comm;
  AppCtx          user;
  FILE            *fp;
  PetscBool       end = PETSC_FALSE;
#if (0)
  EPSType         type;
  PetscReal       tol;
  PetscInt        its, nev, maxit;
#endif
  char            *computeCmd, *CmdStr;

  ierr = SlepcInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = ProcessOpts(comm, &user, &end);CHKERRQ(ierr);
  if (PetscUnlikely(end)) {ierr = SlepcFinalize(); return ierr;}
  PetscStackCall("lammps_open", lammps_open(0, NULL, comm, &user.lmp));LMPERR(user.lmp);

  /*
  lmpBigInt = (size_t) lammps_extract_setting(&user.lmp, (char *) "bigint");
  lmpBigInt = (size_t) lammps_extract_setting(&user.lmp, (char *) "tagint");
  typedef lmpBigInt LammpsBigint;
  typedef lmpTagInt LammpsTagint;
  */
  // We need a better way of checking sizes and definitions for the function pointer.

  /* ------------------------------------------------------
                Setup LAMMPS Simulation
     ------------------------------------------------------ */

  ierr = LammpsReadFileBegin(comm, &user, user.lammpsinfilename, &fp, &computeCmd);CHKERRQ(ierr);

  /* ------------------------------------------------------
                 Set LAMMPS fixexternal Hook
     ------------------------------------------------------ */

  PetscStackCall("lammps_set_fix_external_callback",lammps_set_fix_external_callback(user.lmp, user.feid, (FixExternalFnPtr) &ComputeTotalEnergy, (void *) &user));LMPERR(user.lmp);

  /* ------------------------------------------------------
                Setup Hamiltonian
     ------------------------------------------------------ */

  ierr = LammpsForceNeighListConstruction(&user);CHKERRQ(ierr);
  ierr = LammpsNeighborViewFromOptions(&user, comm);CHKERRQ(ierr);
  ierr = HamiltonianCreate(&user, comm, &user.H);CHKERRQ(ierr);
  ierr = HamiltonianUpdate(&user, user.H);CHKERRQ(ierr);

  /* ------------------------------------------------------
              Create Eigensolver and Set Options
     ------------------------------------------------------ */

  ierr = EPSCreate(comm, &user.eps);CHKERRQ(ierr);
  ierr = EPSSetProblemType(user.eps, EPS_HEP);CHKERRQ(ierr);
  ierr = EPSSetWhichEigenpairs(user.eps, EPS_ALL);CHKERRQ(ierr);
  ierr = EPSSetInterval(user.eps, -10000, PETSC_MAX_REAL);CHKERRQ(ierr);
  ierr = EPSSetFromOptions(user.eps);CHKERRQ(ierr);
  ierr = EPSSetOperators(user.eps, user.H, NULL);CHKERRQ(ierr);
  ierr = EPSSetUp(user.eps);CHKERRQ(ierr);

  ierr = PetscCalloc1(PETSC_MAX_PATH_LEN, &CmdStr);CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user.lmp, &CmdStr, "fix_modify %s energy no", user.feid);CHKERRQ(ierr);
  ierr = LammpsProcessCommand(user.lmp, &CmdStr, computeCmd);CHKERRQ(ierr);
  ierr = PetscFree(CmdStr);CHKERRQ(ierr);

  /* ------------------------------------------------------
                 Get Info About Solve and Display
     ------------------------------------------------------ */
#if (0)
  ierr = EPSValuesView(user.eps, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = EPSGetIterationNumber(user.eps, &its);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "\nNumber of iterations of the method: %D\n", its);
  ierr = EPSGetType(user.eps, &type);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Solution method: %s\n", type);
  ierr = EPSGetDimensions(user.eps, &nev, NULL, NULL);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Number of requested eigenvalues: %D\n", nev);
  ierr = EPSGetTolerances(user.eps, &tol, &maxit);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Stopping condition: tol=%.4f, maxit=%D\n", tol, maxit);
#endif
  /* ------------------------------------------------------
                        Cleanup
     ------------------------------------------------------ */

  ierr = LammpsReadFileEnd(comm, &user, &fp, &computeCmd);CHKERRQ(ierr);
  ierr = EPSDestroy(&user.eps);CHKERRQ(ierr);
  ierr = MatDestroy(&user.H);CHKERRQ(ierr);
  PetscStackCall("lammps_close", lammps_close(user.lmp));
  ierr = PostProcessOpts(comm, &user);CHKERRQ(ierr);
  ierr = SlepcFinalize();
  return ierr;
}
