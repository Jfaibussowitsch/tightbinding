ALL: tightbinding

TB_INC     = -I${LAMMPS_DIR}/src
TB_LIB     = -L${LAMMPS_DIR}/src -llammps -lslepc -ljpeg -lpng
CFLAGS     = ${TB_INC}
FFLAGS     =
CPPFLAGS   = ${TB_INC}
FPPFLAGS   =
CLEANFILES = tightbinding
CURRENT    = $(shell pwd)
NPROCS     = $(shell )
LMPMODE    = static

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

tightbinding: tightbinding.o
	${CLINKER} -w -o tightbinding tightbinding.o ${TB_LIB} ${PETSC_LIB}
#	${RM} tightbinding.o

lammps:
	-@cd ${LAMMPS_DIR}/src && ${MAKE} purge && ${MAKE} -j ${MAKE_NP} -l ${MAKE_LOAD} mode=${LMPMODE} ${LAMMPS_ARCH}

lammps-%:
	-@cd ${LAMMPS_DIR}/src && ${MAKE} purge && ${MAKE} -j ${MAKE_NP} -l ${MAKE_LOAD} $(@:lammps-%=%)

lammps-clean:
	-@cd ${LAMMPS_DIR}/src && ${MAKE} clean-${LAMMPS_ARCH}

lammps-clean-all:
	-@cd ${LAMMPS_DIR}/src && ${MAKE} clean-all

updateetags:
	-@echo "Generating PETSc Etags"
	-@cd ${PETSC_DIR} && ${MAKE} alletags
	-@echo "Generating SLEPc Etags"
	-@cd ${SLEPC_DIR} && ${MAKE} alletags
	-@echo "Generating LAMMPS Etags"
	-@cd ${LAMMPS_DIR} && find ${LAMMPS_DIR}/src -type f \( -regex ".*\.cpp" \
	-o -regex ".*\.h" -o -regex ".*\.c++" \) -print | xargs etags ${LAMMPS_DIR}/TAGS
	-@etags tightbinding.c ./TAGS
	-@echo "Combining all TAGS"
	-@etags -i ${PETSC_DIR}/TAGS -i ${SLEPC_DIR}/TAGS -i ${LAMMPS_DIR}/TAGS
